#/bin/env ruby

Dir.chdir(__dir__);
$LOAD_PATH.push('lib');
Thread.abort_on_exception = true;

require('bundler/setup');

$HOST='localhost'; # Host
$PORT=2345; # Port

def scan(folder)
	filenames = 'png,jpg,jpeg,gif';
	Dir["#{folder}/*.{#{filenames}}"] + Dir["#{folder}/*/"].map{ |folder| scan folder; }.flatten;
end

$SCAN_FOLDER='./examples/';

$images = scan $SCAN_FOLDER;

puts("Found #{$images.length} images to serve.");

require('server');
require('server/curl');
require('server/image');

server = Server.new($HOST,$PORT);

server.listen('/','/image'){ |request|
	path = $images.sample;
	Server::Image.new(path:path, **request);
};

server.listen('/text','/curl'){ |request|
	# TODO: ignore gifs in path
	path = $images.sample;
	Server::Curl.new(path:path, **request);
};

sleep;
