class Server::Image < Server::Default
	@@mimes = {
		'png' => 'image/png',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'webp' => 'image/webp',
		'gif' => 'image/gif',
	}.each{ |_,v| v.freeze }.freeze;

	def mime(path)
		@@mimes[File.extname(path).split('.').last]||'application/octet-stream';
	end

	#######
	private
	#######
	
	def initialize(path: nil, **_)
		super(**_);

		die if path.nil?;

		File.open(path,'rb'){ |file|
			# Send the status
			status 200;

			# Send the headers
			headers({
				'Content-Type': mime(path),
				'Content-Length': file.size,
				'X-Filename': File.basename(path)
			});

			puts "File path #{path}";

			# Stream the file contents
			IO.copy_stream(file, @socket);
		};

		@socket.close;
	end
end
