require('mini_magick');
require('colors');

class Server::Curl < Server::Default
	@@coltype = :col_true; # Color type (:col_true, :col_256, :col_system)
	@@termsize = [80,24]; # Terminal size
	@@charsize = [1,2]; # Character size
	@@padding = true; # To pad or not to pad?

	def convert(input)
		img = MiniMagick::Image.open("#{input}");

		w,h = @termsize;
		cw,ch = @charsize;

		#puts("#{File.basename(input)}");

		# TODO: This could still be faster if instead of resize we would just read the right pixels (skipping rows and columns and reading only a % of the originals)
		# Account for those magic corrections happening later
		img.resize("#{w*cw}x#{h*ch}");
		# Do some magic corrections because usually the terminal characters are 1x2
		img.resize("#{100/cw}% x #{100/ch}%");

		# Optional: calculate padding (if applicable)
		pw, ph = 0, 0;
		if @padding;
			pw = (w-img.width)/2;
			ph = (h-img.height)/2;

			# Negative padding is 0
			pw = 0 if pw < 0;
			ph = 0 if ph < 0;
		end

		# Convert r,g,b to rgb
		pad_top = "\r\n"*ph;
		@socket.write(pad_top);

		pad_left = ' '*pw;

		rows = img.get_pixels;
		rows.each{ |row|
			@socket.write(pad_left);
			row.each{ |rgb|
				@socket.write(col(@coltype,*rgb)+" ");
			}
			@socket.write("\e[m\r\n");
		}

		@socket.close;
	end

	#######
	private
	#######
	
	def parse_headers(headers)
		color = headers['color'];
		if !color.nil? && !color.empty?
			if (color=="true")
				@coltype = :col_true;
			elsif (color=="256"||color.downcase=="8bit")
				@coltype = :col_256;
			elsif (color=="system"||color=="8")
				@coltype = :col_system;
			end
		end

		@termsize[0] = headers['columns'] if !headers['columns'].nil? && !headers['columns'].empty?;
		@termsize[0] = headers['cols'] if !headers['cols'].nil? && !headers['cols'].empty?;

		@termsize[1] = headers['lines'] if !headers['lines'].nil? && !headers['lines'].empty?;
		@termsize[1] = headers['rows'] if !headers['rows'].nil? && !headers['rows'].empty?;

		size = headers['size'];
		if !size.nil? && !size.empty?
			# Yes I know, and no I don't care, and yes it works
			@termsize[0] = size[/\d+/].to_i;
			@termsize[1] = size.sub(/\d+/,'')[/\d+/].to_i;
		end

		stretch = headers['stretch'];
		if !stretch.nil? && !stretch.empty?
			stretch = headers['stretch'][/[0-9.]+/].to_f;
			if stretch > 1
				@charsize[0] = 1;
				@charsize[1] = stretch.to_i;
			else
				@charsize[0] = (1.0/stretch).to_i;
				@charsize[1] = 1;
			end
		end

		pad = headers['padding'];
		if !pad.nil? && !pad.empty?
			@padding = pad[/^n|^f|0/i].nil?;
		end
	end

	def initialize(path: nil, headers: nil, **_)
		super(headers: headers, **_);

		@coltype = @@coltype;
		@termsize = @@termsize.dup;
		@charsize = @@charsize.dup;
		@padding = @@padding;

		die if path.nil?;

		parse_headers(headers);

		status 200;
		headers(**{ 'Connection': 'close', 'X-Filename': File.basename(path) });

		convert(path);
	end
end
