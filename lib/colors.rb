def col_sys(r,g,b)
	# System color
	r/=128;
	g/=128;
	b/=128;
	return "\e[#{40+b*4+g*2+r}m"
end

def col_256(r,g,b)
	# 256 colors
	r/=51;
	g/=51;
	b/=51;
	return "\e[48;5;#{16+r*36+g*6+b}m";
end

def col_true(r,g,b)
	# True color
	return "\e[48;2;#{r};#{g};#{b}m";
end

# Which color scheme to use?
def col(coltype,*rgb)
	if (coltype==:col_true)
		col_true(*rgb);
	elsif (coltype==:col_256)
		col_256(*rgb);
	else
		col_sys(*rgb);
	end
end
