require('socket');
require('timeout');

class Server
	def listen(*paths)
		raise "Listen needs to be initialized with a block." if !block_given?;
		paths.each{ |path|
			@listens[path.to_s.downcase]=->(headers){yield(headers);};
		};
	end

	#######
	private
	#######

	def initialize(host, port, timeout: 20)
		@server = TCPServer.new(host,port);
		@listens = {};

		# The server needs its own thread so it's not blocking.
		Thread.new{
			puts("Server listening on #{host} on port #{port}");

			loop{
				socket = @server.accept;

				# Spawn a new thread for every incoming connection
				Thread.new{
					request = socket.gets;

					puts("#{request}");

					begin
						Timeout::timeout(timeout){
							# Read and parse request headers
							headers={};
							while true
								header_raw = socket.gets;
								break if header_raw=="\r\n";
								header_raw = header_raw.split(':');
								headers[header_raw.shift.strip.downcase] = header_raw.join(':').strip;
							end

							request = request.split[1].downcase;
							request = '/' if !@listens.key?request;

							@listens[request].({
								socket: socket,
								headers: headers
							});
						}
					rescue Timeout::Error => e
						socket.print("HTTP/1.1 408 Request Timeout\r\nConnection: close\r\n\r\n\r\n");
						socket.close;
					rescue Errno::EPIPE => e
						# Socket has closed.
						socket.close;
					end
				};
			};
		};
	end
end

require('server/default');
