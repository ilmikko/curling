# Curling

This is an image server written in ruby which supports serving images over HTTP, either as binary or in plaintext format.

The server will look at the `user-agent` header and determine which format to use.

For example, clients using `curl` can still see images in their terminal in 8-bit, 16-bit and 32-bit true colours (needs to be passed via an additional header).

## Headers

`Color: (true) | (256 | 8bit) | (system | 8)`

Determines the color scheme to use for text images.

If "true", it uses the ANSI true color (32 bit) standard. Not all terminals support this.

If "256" or "8bit", it uses the ANSI extended color standard.

If "system" or "8", it uses the regular terminal colors, which should work across most platforms.

`Columns: 20`
`Rows: 20`

Determines the size of the image.

If one of the sides is larger than the other, the image will be downscaled.

`Stretch: true`

If this header is present, the image will be stretched to fit the size.

`Padding: true`

If this header is present, the image will be padded to be in the "center" of the given columns and rows.
